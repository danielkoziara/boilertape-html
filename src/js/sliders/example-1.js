import Siema from 'siema'

if (document.querySelector('.wines-slider__container')) {
  const wines = new Siema({
    selector: '.wines-slider__container',
    duration: 500,
    easing: 'ease-out',
    perPage: {
      700: 2,
      768: 3,
      1200: 4,
      1450: 5,
    },
    startIndex: 0,
    threshold: 20,
    loop: true,
    onInit: () => {},
    onChange: () => {},
  })

  setInterval(() => wines.next(), 5000)

  document.querySelector('.wines-slider .siema-prev').addEventListener('click', () => wines.prev())
  document.querySelector('.wines-slider .siema-next').addEventListener('click', () => wines.next())
}
